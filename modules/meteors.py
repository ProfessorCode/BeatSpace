from pgzero.builtins import *
from random import choice, randint
import json


class Meteors:
    def __init__(self):
        self.note_lanes = json.load(open('data/lane_positions.json'))
        self.max_meteors = 5
        self.meteor_speed = 5
        self.meteors = []
        self.taken_lanes = []
        self.collision = sounds.load('collision')

    def load(self):
        while len(self.meteors) < self.max_meteors:
            meteor = Actor('meteor')
            lanes = list(self.note_lanes.keys())
            chosen_lane = choice(lanes)
            chosen_lane_x = self.note_lanes[chosen_lane][1]

            if chosen_lane_x not in self.taken_lanes:
                meteor.pos = chosen_lane_x, randint(-1000, 0)
                self.meteors.append(meteor)
                self.taken_lanes.append(chosen_lane_x)

    def update(self, screen_height):
        for meteor in self.meteors:
            meteor.y += self.meteor_speed

            if meteor.top >= screen_height:
                self.remove(meteor)

    def draw(self):
        for meteor in self.meteors:
            meteor.draw()

    def collided(self, meteor):
        self.remove(meteor)
        self.collision.play()

    def remove(self, meteor):
        self.meteors.remove(meteor)
        self.taken_lanes.remove(meteor.x)
