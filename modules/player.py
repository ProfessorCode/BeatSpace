from pgzero.builtins import *
import json
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


class Player:
    def __init__(self, screen_size):
        self.note_lanes = json.load(open('data/lane_positions.json'))
        self.image = Actor('player')
        self.image.x = self.note_lanes['F'][1]
        self.image.y = screen_size[1] - 200
        self.can_move = False
        self.health = screen_size[0]/2

    def draw(self):
        self.image.draw()

    def move(self, key, mod):
        current_note = self.get_current_note()

        if mod & keymods.LSHIFT and key == keys.SPACE:
            logging.info('moving left')

            if not current_note == 'C' and self.can_move:
                next_x = self.get_next_pos('left')
                animate(self.image, tween='decelerate', duration=0.2, x=next_x)
                tone_code = f'{current_note}4'
                tone.play(tone_code, 0.1)
                logging.debug('moved left')
                self.can_move = False

        elif mod & keymods.RSHIFT and key == keys.SPACE:
            logging.info('moving right')

            if not current_note == 'B' and self.can_move:
                next_x = self.get_next_pos('right')
                animate(self.image, tween='decelerate', duration=0.2, x=next_x)
                tone_code = f'{current_note}4'
                tone.play(tone_code, 0.1)
                logging.debug('moved right')
                self.can_move = False

    def get_next_pos(self, direction):
        if direction == 'left':
            next_x = self.image.x - 143
        elif direction == 'right':
            next_x = self.image.x + 143

        return next_x

    def get_current_note(self):
        for note, (start, mid, end) in self.note_lanes.items():
            logging.debug(f'note = {note}, m = {mid}, x = {self.image.x}')
            if self.image.x == mid:
                current_note = note
                break

        return current_note

    def hit(self, damage):
        self.health -= damage
