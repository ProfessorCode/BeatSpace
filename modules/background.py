from pgzero.builtins import *
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


class Background:
    def __init__(self, screen_height):
        self.y1 = 0
        self.screen_height = screen_height
        self.y2 = -self.screen_height
        self.x = 0

    def scroll(self, scroll_speed):
        self.y1 += scroll_speed
        self.y2 += scroll_speed
        self._rewind()

    def _rewind(self):
        if self.y1 >= self.screen_height:
            self.y1 = -self.screen_height
        elif self.y2 >= self.screen_height:
            self.y2 = -self.screen_height
