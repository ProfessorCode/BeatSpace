# BeatSpace

A rhythm-based game made for PyWeek 34 ("The Red Planet").

[PyWeek 34 Entry](https://pyweek.org/e/ProfessorCode34/)

## Running the Game

Python 3.8 is recommended to run this game.

```
pip3 install pgzero
python3 beatspace.py
```

## Playing The Game

Avoid the incoming meteors. You can only move once per beat, though!

```
LSHIFT+SPACE - Move left
RSHIFT+SPACE - Move right
```

## Copyright Attribution

- Images & Sounds - [Kenney.nl](https://kenney.nl/)

## License

This game is licensed under GPL v3.