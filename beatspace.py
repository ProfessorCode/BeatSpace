from modules.background import Background
from modules.meteors import Meteors
from modules.player import Player
from pgzero.builtins import *
import pgzrun
import time
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logging.disable(logging.CRITICAL)
# logging.disable(logging.DEBUG)

WIDTH = 1001
HEIGHT = 800
TITLE = 'BeatSpace'


def draw():
    screen.clear()
    game.draw_background()
    game.meteors.draw()
    game.beat_meter()
    game.player.draw()
    game.draw_health()


def update():
    game.background.scroll(10)
    game.meteors.load()
    game.meteors.update(HEIGHT)
    game.update_beat()
    game.check_collisions()


def on_key_down(key, mod):
    game.player.move(key, mod)


class BeatSpace:
    def __init__(self):
        self.background = Background(HEIGHT)
        self.meteors = Meteors()
        self.player = Player((WIDTH, HEIGHT))
        self.setup_beat()
        self.setup_meters()

    def setup_meters(self):
        top = HEIGHT - 20
        self.health_bar = Rect(0, top, self.player.health, 20)
        self.health_color = (255, 0, 0)

    def draw_background(self):
        pos1 = self.background.x, self.background.y1
        pos2 = self.background.x, self.background.y2
        screen.blit('background', pos2)
        screen.blit('background', pos1)

    def setup_beat(self):
        self.tempo = sounds.load('tempo')
        self.beat_time = time.time()
        self.beat_status = 'ended'
        self.played_tempo = False

    def update_beat(self):
        interval = time.time() - self.beat_time

        if interval >= 0.7 and self.beat_status == 'ended':
            logging.info(f'1/ i = {interval}, bt = {self.beat_time}')
            self.player.can_move = True
            self.beat_status = 'started'
            self.beat_time = time.time()

        elif self.beat_status == 'started':
            if interval >= 0.2 and not self.played_tempo:
                logging.info('Playing tempo')
                self.tempo.play()
                self.played_tempo = True

            elif interval >= 0.7 and self.played_tempo:
                self.beat_status = 'ended'
                self.beat_time = time.time()
                self.played_tempo = False

    def beat_meter(self):
        if self.beat_status == 'started':
            r = 25
            color = (255, 0, 0)
        elif self.beat_status == 'ended':
            r = 15
            color = (0, 0, 255)

        pos = self.player.note_lanes['F'][1], 30
        screen.draw.filled_circle(pos, r, color)

    def check_collisions(self):
        for meteor in self.meteors.meteors:
            if meteor.colliderect(self.player.image):
                self.player.hit(50)
                self.meteors.collided(meteor)
                logging.info(f'health = {self.player.health}')

    def draw_health(self):
        screen.draw.filled_rect(self.health_bar, self.health_color)
        animate(
            self.health_bar, tween='bounce_start_end',
            duration=0.5, width=self.player.health
        )


game = BeatSpace()
pgzrun.go()
